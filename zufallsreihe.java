
/**
 * Beschreiben Sie hier die Klasse zahlenreihe.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class zufallsreihe
{
    private int[] daten;
    int anzahl;
 
    public zufallsreihe(int anzahl)
    {
        this.anzahl = anzahl;
        daten = new int[anzahl];
        for (int i = 0; i < daten.length; i++)
        {
            // Für manche Aufgaben sollte man die 6 durch z.B. 1000 ersetzen
            daten[i] = getZufallszahl(6);
        }
    }
 
    public int aufgabe01Summe()
    {
        return 0;
    }
 
    public int aufgabe02ZaehleNullen()
    {
        return 0;
    }
 
    public int aufgabe03FindeLetzteNull()
    {
        return 0;
    }
 
    public int aufgabe04FindeErsteNull()
    {
        return 0;
    }
 
    public boolean aufgabe05Enthaelt1()
    {
        return false;
    }
 
    public boolean aufgabe06Enthaelt2Und5()
    {
        return false;
    }
 
    public boolean aufgabe07EnthaeltFixpunkt()
    {
        return false;
    }
 
    public int aufgabe08ZaehleWiederholungen()
    {
        return 0;
    }
 
    public int aufgabe09ZaehleDreierWiederholungen()
    {
        return 0;
    }
 
    public int aufgabe10LaengsteSerie()
    {
        return 0;
    }
 
    public int aufgabe11Zweitgroesste()
    {
        return 0;
    }
 
    public void aufgabe12Plus1()
    {
 
    }
 
    public void aufgabe13NullZuHundert()
    {
 
    }
 
    public void aufgabe14Rotation()
    {
 
    }
 
    public void aufgabe15Umdrehen()
    {
 
    }
 
 
     /** dient zum Anzeigen der Reihung am Bildschirm;
     * kann durch INSPECT ersetzt werden */
    public void anzeigen() {
        for (int i=0; i< anzahl; i++) {
             System.out.println( i + " :  " + daten[i]); 
        }
    }    
 
    /**
     * Gibt eine Zufallszahl zwischen 0 und grenze-1 zurück.
     */
    private int getZufallszahl(int grenze)
    {
        return (int)(grenze*Math.random()+1);
    }
}
